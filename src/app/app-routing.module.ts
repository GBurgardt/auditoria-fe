import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ConsultaMatrizComponent } from './components/consulta-matriz/consulta-matriz.component';
import { LoginComponent } from './components/login/login.component';
import { AuditoriaComponent } from './components/auditoria/auditoria.component';
import { NuevoOcComponent } from './components/consulta-matriz/components/oc-table/components/nuevo-oc/nuevo-oc.component';
import { EditarOcComponent } from './components/consulta-matriz/components/oc-table/components/editar-oc/editar-oc.component';
import { NuevoRrComponent } from './components/consulta-matriz/components/oc-table/components/nuevo-rr/nuevo-rr.component';
import { NuevoAcComponent } from './components/consulta-matriz/components/oc-table/components/nueva-ac/nuevo-ac.component';
import { EditarRrComponent } from './components/consulta-matriz/components/oc-table/components/editar-rr/editar-rr.component';
import { EditarAcComponent } from './components/consulta-matriz/components/oc-table/components/editar-ac/editar-ac.component';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'auditoria',
        component: AuditoriaComponent,
        children: [
            {
                path: 'consulta-matriz',
                component: ConsultaMatrizComponent
            },
            {
                path: 'consulta-matriz/nuevo-oc',
                component: NuevoOcComponent
            },
            {
                path: 'consulta-matriz/editar-oc/:mto_n_objetivo',
                component: EditarOcComponent
            },
            {
                path: 'consulta-matriz/nuevo-rr',
                component: NuevoRrComponent
            },
            {
                path: 'consulta-matriz/editar-rr/:mtr_n_riesgo',
                component: EditarRrComponent
            },
            {

                path: 'consulta-matriz/nuevo-ac',
                component: NuevoAcComponent
            },
            {
                path: 'consulta-matriz/editar-ac/:mto_n_objetivo/:mtr_n_riesgo/:mta_n_actividad',
                component: EditarAcComponent
            },
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full'
            },
        ]
    },
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: LoginComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

