import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { ConsultaMatrizComponent } from './components/consulta-matriz/consulta-matriz.component';

import { LoginComponent } from './components/login/login.component';
import { AuditoriaComponent } from './components/auditoria/auditoria.component';

import { AsideMenuComponent } from './components/reusable/aside-menu/aside-menu.component';
import { BasicModalComponent } from './components/reusable/basic-modal/basic-modal.component';
import { MainHeaderComponent } from './components/reusable/main-header/main-header.component';
import { OcTableComponent } from './components/consulta-matriz/components/oc-table/oc-table.component';

import { NuevoOcComponent } from './components/consulta-matriz/components/oc-table/components/nuevo-oc/nuevo-oc.component';
import { EditarOcComponent } from './components/consulta-matriz/components/oc-table/components/editar-oc/editar-oc.component';
import { NuevoRrComponent } from './components/consulta-matriz/components/oc-table/components/nuevo-rr/nuevo-rr.component';
import { EditarRrComponent } from './components/consulta-matriz/components/oc-table/components/editar-rr/editar-rr.component';
import { UtilsService } from './services/utils.service';


import { NuevoAcComponent } from './components/consulta-matriz/components/oc-table/components/nueva-ac/nuevo-ac.component';
import { EditarAcComponent } from './components/consulta-matriz/components/oc-table/components/editar-ac/editar-ac.component';
import { RrTableComponent } from './components/consulta-matriz/components/oc-table/components/rr-table/rr-table.component';
import { TrTableComponent } from './components/consulta-matriz/components/oc-table/components/rr-table/components/ac-table/components/tr-table/tr-table.component';
import { AcTableComponent } from './components/consulta-matriz/components/oc-table/components/rr-table/components/ac-table/ac-table.component';




///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////

@NgModule({
    declarations: [
        AppComponent,
        ConsultaMatrizComponent,
        AsideMenuComponent,
        LoginComponent,
        AuditoriaComponent,
        BasicModalComponent,
        MainHeaderComponent,
        OcTableComponent,
        RrTableComponent,
        TrTableComponent,
        AcTableComponent,
        NuevoOcComponent,
        EditarOcComponent,
        NuevoRrComponent,
        EditarRrComponent,
        NuevoAcComponent,
        EditarAcComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [
        AuthService,
        UtilsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
