import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { restResources } from 'src/app/constants/restResources';
import { AccionControl } from 'src/app/models/accionControl';

@Component({
    selector: 'app-editar-ac',
    templateUrl: './editar-ac.component.html',
    styleUrls: ['./editar-ac.component.scss']
})
export class EditarAcComponent implements OnInit {

    recurso: AccionControl;
    dataModal = null;

    constructor(
        private route: ActivatedRoute,
        private authService: AuthService,
        private localStorageService: LocalStorageService,
        private router: Router
    ) {
        this.route.params.subscribe(params => {
            
            this.authService.getResource(restResources.ACCIONES_CONTROL, {
                mta_c_empresa: this.localStorageService.getEmpresa(),
                mta_n_auditoria: this.localStorageService.getNroAuditoria(),
                mta_n_objetivo: params && params.mto_n_objetivo ? params.mto_n_objetivo : null,
                mta_n_riesgo: params && params.mtr_n_riesgo ? params.mtr_n_riesgo : null,
                mta_n_actividad: params && params.mta_n_actividad ? params.mta_n_actividad : null
            }).subscribe(
                (rec: any) =>{
                    debugger;
                    this.recurso = new AccionControl(rec.data)
                }
            )
        }
        );
    }

    ngOnInit() { }


    onClickGuardar = () => {

        this.recurso;
        debugger;

        this.authService.putResource(restResources.ACCIONES_CONTROL, this.recurso)
            .subscribe((resp: any) => 
                this.dataModal = {
                    title: resp.infoModal.title,
                    innerText: resp.infoModal.innerText,
                    acceptCallback: () => 
                        this.router.navigate(['/auditoria/consulta-matriz'], {
                            queryParamsHandling: 'preserve'
                        })
                }
            )
    }

}
