import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarOcComponent } from './editar-oc.component';

describe('EditarOcComponent', () => {
  let component: EditarOcComponent;
  let fixture: ComponentFixture<EditarOcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarOcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarOcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
