import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UtilsService } from 'src/app/services/utils.service';
import { restResources } from 'src/app/constants/restResources';
import { ObjetivoControl } from 'src/app/models/objetivoControl';



@Component({
    selector: 'app-editar-oc',
    templateUrl: './editar-oc.component.html',
    styleUrls: ['./editar-oc.component.scss']
})
export class EditarOcComponent implements OnInit {

    recurso: ObjetivoControl;

    categorias: Observable<any[]>;
    riesgos: Observable<any[]>;

    // Se usa para mostar info en un modal
    dataModal = null;

    constructor(
        private route: ActivatedRoute,
        private authService: AuthService,
        private localStorageService: LocalStorageService,
        public utilsService: UtilsService,
        private router: Router
    ) {
        this.route.params.subscribe(params =>
            this.authService.getResource(restResources.OBJETIVOS_CONTROL, {
                mt_c_empresa: this.localStorageService.getEmpresa(),
                mt_n_auditoria: this.localStorageService.getNroAuditoria(),
                mto_n_objetivo: params && params.mto_n_objetivo ? params.mto_n_objetivo : null
            }).subscribe(
                (rec: any) => this.recurso = new ObjetivoControl(rec.data)
            )
        );
    }

    /**
     * Cargo listas desplegables
     */
    ngOnInit() {
        this.categorias = this.authService.getResource('categorias', {
            pco_c_empresa: this.localStorageService.getEmpresa()
        }).pipe(
            map((a: any) => a.data)
        );

        this.riesgos = this.authService.getResource('riesgos', {
            prsg_c_empresa: this.localStorageService.getEmpresa()
        }).pipe(
            map((a: any) => a.data)
        );
    }


    onClickGuardar = () => {
        this.authService.putResource(restResources.OBJETIVOS_CONTROL, this.recurso)
            .subscribe((resp: any) => 
                this.dataModal = {
                    title: resp.infoModal.title,
                    innerText: resp.infoModal.innerText,
                    acceptCallback: () => 
                        this.router.navigate(['/auditoria/consulta-matriz'], {
                            queryParamsHandling: 'preserve'
                        })
                }
            )
    }

}
