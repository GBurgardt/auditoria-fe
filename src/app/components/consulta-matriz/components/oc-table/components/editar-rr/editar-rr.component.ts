import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { restResources } from 'src/app/constants/restResources';
import { RiesgoRelacionado } from 'src/app/models/riesgoRelacionado';

@Component({
    selector: 'app-editar-rr',
    templateUrl: './editar-rr.component.html',
    styleUrls: ['./editar-rr.component.scss']
})
export class EditarRrComponent implements OnInit {

    recurso: RiesgoRelacionado;
    dataModal = null;

    constructor(
        private route: ActivatedRoute,
        private authService: AuthService,
        private localStorageService: LocalStorageService,
        private router: Router
    ) {
        this.route.params.subscribe(params =>
            this.authService.getResource(restResources.RIESGOS_RELACIONADOS, {
                mtr_c_empresa: this.localStorageService.getEmpresa(),
                mtr_n_auditoria: this.localStorageService.getNroAuditoria(),
                mtr_n_riesgo: params && params.mtr_n_riesgo ? params.mtr_n_riesgo : null
            }).subscribe(
                (rec: any) => this.recurso = new RiesgoRelacionado(rec.data)
            )
        );
    }

    ngOnInit() { }

    onClickGuardar = () => {
        this.authService.putResource(restResources.RIESGOS_RELACIONADOS, this.recurso)
            .subscribe((resp: any) => 
                this.dataModal = {
                    title: resp.infoModal.title,
                    innerText: resp.infoModal.innerText,
                    acceptCallback: () => 
                        this.router.navigate(['/auditoria/consulta-matriz'], {
                            queryParamsHandling: 'preserve'
                        })
                }
            )
    }

}
