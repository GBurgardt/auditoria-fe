import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AccionControl } from 'src/app/models/accionControl';
import { restResources } from 'src/app/constants/restResources';

@Component({
    selector: 'app-nuevo-ac',
    templateUrl: './nuevo-ac.component.html',
    styleUrls: ['./nuevo-ac.component.scss']
})
export class NuevoAcComponent {

    recurso: AccionControl = new AccionControl();

    dataModal = null;

    constructor(
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        // Busco los queryparams (por query params está viniendo los datos que identifican a la matriz padre)
        this.route.queryParams
            .subscribe(respQuery => {
                if (respQuery && Object.keys(respQuery).length > 0) {
                    this.recurso.mta_n_objetivo = respQuery.mto_n_objetivo;
                    this.recurso.mta_n_riesgo = respQuery.mtr_n_riesgo;
                    debugger;
                }
            });
    }

    /**
     * Guardar objetivo de control
     */
    onClickGrabar = () => {
        this.recurso;
        debugger;
        this.authService.postResource(restResources.ACCIONES_CONTROL, this.recurso)
            .subscribe((resp: any) => 
                this.dataModal = {
                    title: resp.infoModal.title,
                    innerText: resp.infoModal.innerText,
                    acceptCallback: () => 
                        this.router.navigate(['/auditoria/consulta-matriz'], {
                            queryParamsHandling: 'preserve'
                        })
                }
            )
    }

}
