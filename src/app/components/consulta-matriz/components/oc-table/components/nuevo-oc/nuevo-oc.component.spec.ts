import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoOcComponent } from './nuevo-oc.component';

describe('NuevoOcComponent', () => {
  let component: NuevoOcComponent;
  let fixture: ComponentFixture<NuevoOcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoOcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoOcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
