import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { map } from 'rxjs/operators';

import { ActivatedRoute, Router } from '@angular/router';
import { restResources } from 'src/app/constants/restResources';

import { ObjetivoControl } from 'src/app/models/objetivoControl';

@Component({
    selector: 'app-nuevo-oc',
    templateUrl: './nuevo-oc.component.html',
    styleUrls: ['./nuevo-oc.component.scss']
})
export class NuevoOcComponent implements OnInit {

    /**
     * Recurso (objetivo de control).
     * Seteo por defecto:
     * - categoria 1
     * - riesgo 1
     */
    recurso: ObjetivoControl = new ObjetivoControl();

    categorias: Observable<any[]>;
    riesgos: Observable<any[]>;

    // Se usa para mostar info en un modal
    dataModal = null;

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        // Busco los queryparams (por query params está viniendo los datos que identifican a la matriz padre)
        this.route.queryParams
            .subscribe(respQuery => {
                if (respQuery && Object.keys(respQuery).length > 0) {

                    // Actualizo el ciclo (que es lo que identifica a la matriz padre?)
                    this.recurso.mto_n_ciclo = respQuery.mto_n_ciclo;
                }
            });
    }


    /**
     * Cargo listas dessplegables
     */
    ngOnInit() {
        this.categorias = this.authService.getResource('categorias', {
            pco_c_empresa: this.localStorageService.getEmpresa()
        }).pipe(
            map((a: any) => a.data)
        );

        this.riesgos = this.authService.getResource('riesgos', {
            prsg_c_empresa: this.localStorageService.getEmpresa()
        }).pipe(
            map((a: any) => a.data)
        );
    }


    /**
     * Guardar objetivo de control
     */
    onClickGrabar = () => 
        this.authService.postResource(restResources.OBJETIVOS_CONTROL, this.recurso)
            .subscribe((resp: any) => 
                this.dataModal = {
                    title: resp.infoModal.title,
                    innerText: resp.infoModal.innerText,
                    acceptCallback: () => 
                        this.router.navigate(['/auditoria/consulta-matriz'], {
                            queryParamsHandling: 'preserve'
                        })
                }
            )

}
