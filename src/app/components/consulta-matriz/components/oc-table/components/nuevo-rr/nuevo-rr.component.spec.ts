import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoRrComponent } from './nuevo-rr.component';

describe('NuevoRrComponent', () => {
  let component: NuevoRrComponent;
  let fixture: ComponentFixture<NuevoRrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoRrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoRrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
