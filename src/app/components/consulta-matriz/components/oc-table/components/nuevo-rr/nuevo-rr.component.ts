import { Component, OnInit } from '@angular/core';
import { RiesgoRelacionado } from 'src/app/models/riesgoRelacionado';
import { AuthService } from 'src/app/services/auth.service';
import { restResources } from 'src/app/constants/restResources';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-nuevo-rr',
    templateUrl: './nuevo-rr.component.html',
    styleUrls: ['./nuevo-rr.component.scss']
})
export class NuevoRrComponent implements OnInit {

    recurso: RiesgoRelacionado = new RiesgoRelacionado();

    dataModal = null;

    constructor(
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        // Busco los queryparams (por query params está viniendo los datos que identifican a la matriz padre)
        this.route.queryParams
            .subscribe(respQuery => {
                if (respQuery && Object.keys(respQuery).length > 0) {
                    // Actualizo el ciclo (que es lo que identifica a la matriz padre?)
                    this.recurso.mtr_n_objetivo = respQuery.mto_n_objetivo;
                }
            });
    }

    ngOnInit() { }

    /**
     * Guardar objetivo de control
     */
    onClickGrabar = () => 
        this.authService.postResource(restResources.RIESGOS_RELACIONADOS, this.recurso)
            .subscribe((resp: any) => 
                this.dataModal = {
                    title: resp.infoModal.title,
                    innerText: resp.infoModal.innerText,
                    acceptCallback: () => 
                        this.router.navigate(['/auditoria/consulta-matriz'], {
                            queryParamsHandling: 'preserve'
                        })
                }
            )

}
