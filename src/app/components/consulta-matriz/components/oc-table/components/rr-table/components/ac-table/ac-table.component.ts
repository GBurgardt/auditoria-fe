import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

import { LocalStorageService } from 'src/app/services/local-storage.service';
import { restResources } from 'src/app/constants/restResources';

import { RowConsultaMatriz } from 'src/app/models/rowConsultaMatriz';


@Component({
    selector: 'ac-table',
    templateUrl: './ac-table.component.html',
    styleUrls: ['./ac-table.component.scss']
})
export class AcTableComponent implements OnInit {

    // Data de las oc
    @Input() columns;
    @Input() data;
    @Input() updateMatricesTrigger;

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService
    ) { }

    ngOnInit() { }

    onClickDelete = (child: RowConsultaMatriz) => {
        debugger;
        this.authService.deleteResource(
            restResources.ACCIONES_CONTROL,
            [child.mta_n_actividad.toString()],
            {
                p_c_empresa: this.localStorageService.getEmpresa(),
                p_n_auditoria: this.localStorageService.getNroAuditoria(),
                p_n_objetivo: child.mto_n_objetivo,
                p_n_riesgo: child.mtr_n_riesgo
            }
        ).subscribe(a => {
            this.updateMatricesTrigger()
        })
    }

}
