import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'tr-table',
    templateUrl: './tr-table.component.html',
    styleUrls: ['./tr-table.component.scss']
})
export class TrTableComponent implements OnInit {

    // Data de las oc
    @Input() columns;
    @Input() data;

    constructor() { }

    ngOnInit() {
    }

}
