import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { restResources } from 'src/app/constants/restResources';
import { RiesgoRelacionado } from 'src/app/models/riesgoRelacionado';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
    selector: 'rr-table',
    templateUrl: './rr-table.component.html',
    styleUrls: ['./rr-table.component.scss']
})
export class RrTableComponent implements OnInit {

    // Data de las oc
    @Input() columns;
    @Input() data;
    @Input() updateMatricesTrigger;

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService
    ) { }

    ngOnInit() { }

    onClickDelete = (child: RiesgoRelacionado) => {
        // If child es RR... bla bla bla
        this.authService.deleteResource(
            restResources.RIESGOS_RELACIONADOS,
            [child.mtr_n_riesgo.toString()],
            {
                p_c_empresa: this.localStorageService.getEmpresa(),
                p_n_auditoria: this.localStorageService.getNroAuditoria()
            }
        ).subscribe(a => {
            this.updateMatricesTrigger()
        })
    }

}
