export const columnsDefOc = [
    { headerName: 'Tipo', abbr: 'Tip', field: 't_registro' }, // OK
    { headerName: 'Papeles', abbr: 'Pape', field: 'papeles' }, // Al parecer es una imagen, por ahora no pongo nada
    { headerName: 'Descripcion', abbr: 'Desc', field: 'co_descrip' }, 
    { headerName: 'Control', abbr: 'Cont', field: 'control' }, 
    // { headerName: 'Descripcion del Control', abbr: 'Desc Cont', field: 'mta_existe_descrip' }, // MTA_EXISTE_DESCRIP -> NO viene en el storeProcedure
    { headerName: 'Descripcion del Control', abbr: 'Desc Cont', field: 'mto_objetivo' }, // MTA_EXISTE_DESCRIP -> NO viene en el storeProcedure
    { headerName: 'Normas', abbr: 'Norm', field: 'mta_norma' }, // OK
    { headerName: 'Descripcion', abbr: 'Desc', field: 'mta_norma_descrip' }, // OK
    { headerName: 'Control Adecuado', abbr: 'Cont Adec', field: 'mta_ac_estado' }, // OK
    // { headerName: 'Crear', abbr: 'Crear', field: 'crear' }, // OK
];