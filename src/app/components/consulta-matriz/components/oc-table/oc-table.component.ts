import { Component, OnInit, Input } from '@angular/core';

import { AuthService } from 'src/app/services/auth.service';
import { restResources } from 'src/app/constants/restResources';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { RowConsultaMatriz } from 'src/app/models/rowConsultaMatriz';

@Component({
    selector: 'oc-table',
    templateUrl: './oc-table.component.html',
    styleUrls: ['./oc-table.component.scss']
})
export class OcTableComponent implements OnInit {

    // Data de las oc
    @Input() columns;
    @Input() data;
    @Input() updateMatricesTrigger;

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService
    ) { }

    ngOnInit() { }

    onClickDelete = (oc: RowConsultaMatriz) => {

        this.authService.deleteResource(
            restResources.OBJETIVOS_CONTROL,
            [oc.mto_n_objetivo.toString()],
            {
                pmto_c_empresa: this.localStorageService.getEmpresa(),
                pmto_n_auditoria: this.localStorageService.getNroAuditoria()
            }
        ).subscribe(a=>{
            this.updateMatricesTrigger()
        })
    }

}
