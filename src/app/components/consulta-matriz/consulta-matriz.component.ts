import { Component, OnInit, ViewChild } from '@angular/core';

import { AuthService } from 'src/app/services/auth.service';

import { tiposMatrices } from 'src/app/constants/tiposMatrices';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { columnsDefOc } from './components/oc-table/oc-columns-defs';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';
import { FiltroConsultaMatriz } from 'src/app/models/filtroConsultaMatriz';
import { RowConsultaMatriz } from 'src/app/models/rowConsultaMatriz';


@Component({
    selector: 'consulta-matriz',
    templateUrl: './consulta-matriz.component.html',
    styleUrls: ['./consulta-matriz.component.scss']
})
export class ConsultaMatrizComponent implements OnInit {

    ////////////////////////////////////////////////
    //////////////// Matrices //////////////////////
    ////////////////////////////////////////////////
    objetivosControl: RowConsultaMatriz[];
    columnsOc = columnsDefOc;

    ////////////////////////////////////////////////
    //////////////// Banderas //////////////////////
    ////////////////////////////////////////////////
    showFiltros = true;

    ////////////////////////////////////////////////
    //////////////// Data Listas ///////////////////
    ////////////////////////////////////////////////
    ciclos: any[] = [];
    subCiclos: any[] = [];
    actividades: any[] = [];
    componentes: any[] = [];

    ////////////////////////////////////////////////
    /////////////////// Otros //////////////////////
    ////////////////////////////////////////////////
    filtros: FiltroConsultaMatriz = new FiltroConsultaMatriz();

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService,
        private route: ActivatedRoute,
        private utilsService: UtilsService,
        private router: Router
    ) { }

    /**
     * Cargo listas, agarro query params y otras cosas iniciales
     */
    ngOnInit() {
        
        this.updateCiclos();

        // Busco si hay query params (que son filtros anteriores guardados)
        this.route.queryParams
            .subscribe((filtrosQuery: any) => {
                if (filtrosQuery && Object.keys(filtrosQuery).length > 0) {
                    // Actualizo filtros
                    this.filtros = new FiltroConsultaMatriz({
                        cl_c_ciclo: filtrosQuery.cl_c_ciclo,
                        cls_n_subciclo: filtrosQuery.cls_n_subciclo,
                        mt_actividad: filtrosQuery.mt_actividad,
                        mt_componente_ci: filtrosQuery.mt_componente_ci
                    })

                    // Refresco todo
                    this.updateSubciclos(this.filtros.cl_c_ciclo);
                    this.updateActividades(this.filtros.cls_n_subciclo);
                    this.updateComponentes(this.filtros.cls_n_subciclo);
                    this.updateMatrices();
                    // debugger;
                }  
            });

        // Me subscribo a los cambios de la Auditoria global. Y voy actualziando todo de acuerdo a ella
        this.localStorageService.getAuditoriaSubject().subscribe(nuevaAudi => {
            this.updateCiclos();
            this.updateSubciclos(this.filtros.cl_c_ciclo);
            this.updateActividades(this.filtros.cls_n_subciclo);
            this.updateComponentes(this.filtros.cls_n_subciclo);
            this.updateMatrices();
        })
    }

    updateCiclos = () => {
        // Cargo listas
        this.authService.getResource('ciclos', {
            mt_c_empresa: this.localStorageService.getEmpresa(),
            mt_n_auditoria: this.localStorageService.getNroAuditoria()
        }).subscribe(
            (resp: any) => this.ciclos = resp.data
        );
    }
    

    updateSubciclos = (cl_c_ciclo) => this.filtros && 
            this.authService.getResource('sub-ciclos', {
                mt_c_empresa: this.localStorageService.getEmpresa(),
                mt_n_auditoria: this.localStorageService.getNroAuditoria(),
                mt_c_ciclo: cl_c_ciclo
            }).subscribe(
                (resp: any) => this.subCiclos = resp.data
            )
    

    updateActividades = (cls_n_subciclo) => this.filtros &&
        this.authService.getResource('actividades', {
            mt_c_empresa: this.localStorageService.getEmpresa(),
            mt_n_auditoria: this.localStorageService.getNroAuditoria(),
            mt_c_ciclo: this.filtros.cl_c_ciclo,
            mt_n_subciclo: cls_n_subciclo
        }).subscribe(
            (resp: any) => this.actividades = resp.data
        )

    updateComponentes = (cls_n_subciclo) => this.filtros &&
        this.authService.getResource('componentes', {
            mt_c_empresa: this.localStorageService.getEmpresa(),
            mt_n_auditoria: this.localStorageService.getNroAuditoria(),
            mt_c_ciclo: this.filtros.cl_c_ciclo,
            mt_n_subciclo: cls_n_subciclo
        }).subscribe(
            (resp: any) => this.componentes = resp.data
        )

    updateMatrices = () => this.filtros && 
        this.authService.getResource('matrices', {
            c_empresa: this.localStorageService.getEmpresa(),
            n_auditoria: this.localStorageService.getNroAuditoria(),
            c_ciclo: this.filtros.cl_c_ciclo,
            n_subciclo: this.filtros.cls_n_subciclo,
            actividad: this.filtros.mt_actividad ? this.filtros.mt_actividad : '',
            componente_ci: this.filtros.mt_componente_ci ? this.filtros.mt_componente_ci : '',
        }).subscribe(
            (resp: any) => this.objetivosControl = resp.data.map(oc => new RowConsultaMatriz(oc))
        )


    /**
     * Actualiza los queryParams. Y la matriz se actualiza sola, xq se escucha los queryparams todo el tiempo
     */
    onClickBuscar = () => {
        // Actualizo queryParams (se dispara el subscribe de arriba y actualiza matrices solo)
        this.router.navigate(
            [],
            {
                queryParams: {
                    cl_c_ciclo: this.filtros.cl_c_ciclo,
                    cls_n_subciclo: this.filtros.cls_n_subciclo,
                    mt_actividad: this.filtros.mt_actividad,
                    mt_componente_ci: this.filtros.mt_componente_ci
                }
            });
    }
}