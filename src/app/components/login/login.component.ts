import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    // Se usa para mostar info en un modal
    dataModal = null;

    // Guarda data del usuario a loguearse
    usuario: {
        empresa: string,
        nombre: string,
        clave: string
    } = { empresa: null, nombre: null, clave: null};

    constructor(
        private loginService: LoginService,
        private router: Router
    ) { }

    ngOnInit() { }

    login = () => {

        this.loginService.processLogin(this.usuario)
            .subscribe(resp => {

                if (resp.isOkey) {
                    this.router.navigate(['/auditoria/consulta-matriz']);
                } else {
                    this.dataModal = resp.dataModal;
                }

            })

    }

    modalAcceptCallback = () => {
        this.dataModal = null
    }


}
