import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aside-menu',
  templateUrl: './aside-menu.component.html',
  styleUrls: ['./aside-menu.component.scss']
})
export class AsideMenuComponent implements OnInit {

  // Estados de los menus (abierto/cerrado)
  toggleMenus: any = { mantenimiento: false, auditorias: false, listados: false, usuarios: false, ayuda: false }

  constructor() { }

  ngOnInit() { }

}
