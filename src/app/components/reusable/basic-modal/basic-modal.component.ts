import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-basic-modal',
    templateUrl: './basic-modal.component.html',
    styleUrls: ['./basic-modal.component.scss']
})
export class BasicModalComponent implements OnInit {
    @Input() isActive = false;

    @Input() title;
    @Input() innerText;
    @Input() acceptCallback;

    constructor() { }

    ngOnInit() {
        setTimeout(() => {
            console.log(this.isActive)
        }, 400);
    }

}
