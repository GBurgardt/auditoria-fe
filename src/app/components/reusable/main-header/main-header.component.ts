import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Auditoria } from 'src/app/models/auditoria';

@Component({
    selector: 'app-main-header',
    templateUrl: './main-header.component.html',
    styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {
    
    auditorias: Auditoria[] = [];
    au_n_auditoria_select: number;

    constructor(
        private router: Router,
        private authService: AuthService,
        public localStorageService: LocalStorageService
    ) { }

    ngOnInit() {
        //////////////// Cargo Listas ///////////////////
        this.authService.getResource('auditorias', {
            au_c_empresa: this.localStorageService.getEmpresa()
        }).subscribe(
            (resp: any) => this.auditorias = resp.data
        );

        this.au_n_auditoria_select = Number(this.localStorageService.getNroAuditoria());
    }

    exit = () => {
        this.router.navigate(['/login']);
        // Borrar de localstorage
    }


    /**
     * Setea la nueva auditoria seleccionada en el localStorage
     */
    onChangeAuditoria = (audiSelect: Auditoria) => {
        // Seteo auditoria en localStorage
        this.localStorageService.setObject('auditoria', audiSelect);
        // this.localStorageService.setObject('au_t_auditoria', audiSelect.au_t_auditoria);

        // nexteo auditoria nueva en el Subject para que puedan escuchar cambios
        this.localStorageService.getAuditoriaSubject().next(audiSelect);
    }

}
