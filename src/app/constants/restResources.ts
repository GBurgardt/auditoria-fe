export const restResources = {
    OBJETIVOS_CONTROL: 'objetivos-control',
    RIESGOS_RELACIONADOS: 'riesgos-relacionados',
    ACCIONES_CONTROL: 'acciones-control'
};