export const tiposMatrices = {
    OC: {
        name: 'OC',
        descripcion: 'Objetivo de Control',
        classColor: 'green-row'
    },
    AC: {
        name: 'AC',
        descripcion: 'Accion de Control',
        classColor: 'gray-row'
    },
    TR: {
        name: 'TR',
        descripcion: 'Tarea Relacionada',
        classColor: 'light-blue-row'
    },
    RR: {
        name: 'RR',
        descripcion: 'Riesgo Relacionado',
        classColor: 'brown-row'
    },
};