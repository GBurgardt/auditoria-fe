
export class AccionControl {
    mta_c_empresa: number;
    mta_n_auditoria: number;
    mta_n_actividad: number;
    mta_n_objetivo: number;
    mta_actividad: string;
    mta_existe: string;
    mta_existe_descrip: string;
    mta_norma: string;
    mta_norma_descrip: string;
    mta_referencia: string;
    mta_ac_estado: string;
    mta_usuario: string;
    mta_f_graba: string;
    mta_n_riesgo: number;
    mta_existe_res: string;
    mta_existe_descrip_res: string

    constructor(accionControl?: {
        mta_c_empresa: number;
        mta_n_auditoria: number;
        mta_n_actividad: number;
        mta_n_objetivo: number;
        mta_actividad: string;
        mta_existe: string;
        mta_existe_descrip: string;
        mta_norma: string;
        mta_norma_descrip: string;
        mta_referencia: string;
        mta_ac_estado: string;
        mta_usuario: string;
        mta_f_graba: string;
        mta_n_riesgo: number;
        mta_existe_res: string;
        mta_existe_descrip_res: string
    }) {
        if (accionControl) {
            this.mta_c_empresa = accionControl.mta_c_empresa;
            this.mta_n_auditoria = accionControl.mta_n_auditoria;
            this.mta_n_actividad = accionControl.mta_n_actividad;
            this.mta_n_objetivo = accionControl.mta_n_objetivo;
            this.mta_actividad = accionControl.mta_actividad;
            this.mta_existe = accionControl.mta_existe;
            this.mta_existe_descrip = accionControl.mta_existe_descrip;
            this.mta_norma = accionControl.mta_norma;
            this.mta_norma_descrip = accionControl.mta_norma_descrip;
            this.mta_referencia = accionControl.mta_referencia;
            this.mta_ac_estado = accionControl.mta_ac_estado;
            this.mta_usuario = accionControl.mta_usuario;
            this.mta_f_graba = accionControl.mta_f_graba;
            this.mta_n_riesgo = accionControl.mta_n_riesgo;
            this.mta_existe_res = accionControl.mta_existe_res;
            this.mta_existe_descrip_res = accionControl.mta_existe_descrip_res
        } else {
            this.mta_c_empresa = null;
            this.mta_n_auditoria = null;
            this.mta_n_actividad = null;
            this.mta_n_objetivo = null;
            this.mta_actividad = null;
            this.mta_existe = null;
            this.mta_existe_descrip = null;
            this.mta_norma = null;
            this.mta_norma_descrip = null;
            this.mta_referencia = null;
            this.mta_ac_estado = null;
            this.mta_usuario = null;
            this.mta_f_graba = null;
            this.mta_n_riesgo = null;
            this.mta_existe_res = null;
            this.mta_existe_descrip_res = null
        }
    }

}