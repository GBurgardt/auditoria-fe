
export class Auditoria {
    au_t_auditoria: string;
    au_n_auditoria: number;
    descrip: string

    constructor(auditoria?: {
        au_t_auditoria: string;
        au_n_auditoria: number;
        descrip: string
    }) {
        if (auditoria) {
            this.au_t_auditoria = auditoria.au_t_auditoria;
            this.au_n_auditoria = auditoria.au_n_auditoria;
            this.descrip = auditoria.descrip
        } else {
            this.au_t_auditoria = null;
            this.au_n_auditoria = null;
            this.descrip = null
        }
    }

}