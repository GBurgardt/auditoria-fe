
export class FiltroConsultaMatriz {
    cl_c_ciclo: string;
    cls_n_subciclo: string;
    mt_actividad: string
    mt_componente_ci: string

    constructor(filtroConsultaMatriz?: {
        cl_c_ciclo: string;
        cls_n_subciclo: string;
        mt_actividad: string;
        mt_componente_ci: string;
    }) {
        if (filtroConsultaMatriz) {
            this.cl_c_ciclo = filtroConsultaMatriz.cl_c_ciclo;
            this.cls_n_subciclo = filtroConsultaMatriz.cls_n_subciclo;
            this.mt_actividad = filtroConsultaMatriz.mt_actividad;
            this.mt_componente_ci = filtroConsultaMatriz.mt_componente_ci;
        } else {
            this.cl_c_ciclo = null;
            this.cls_n_subciclo = null;
            this.mt_actividad = null;
            this.mt_componente_ci = null;
        }
    }

}