export class ObjetivoControl {
    mto_c_empresa: number;
    mto_n_auditoria: number;
    mto_n_objetivo: number;
    mto_n_ciclo: number;
    mto_objetivo: string;
    mto_c_categoria: number;
    mto_usuario: string;
    mto_f_graba: string;
    mto_p_maximo: number;
    mto_califica: string;
    mto_valoracion: string;
    mto_c_riesgo: number

    constructor(objetivoControl?: {
        mto_c_empresa: number;
        mto_n_auditoria: number;
        mto_n_objetivo: number;
        mto_n_ciclo: number;
        mto_objetivo: string;
        mto_c_categoria: number;
        mto_usuario: string;
        mto_f_graba: string;
        mto_p_maximo: number;
        mto_califica: string;
        mto_valoracion: string;
        mto_c_riesgo: number
    }) {
        if (objetivoControl) {
            this.mto_c_empresa = objetivoControl.mto_c_empresa;
            this.mto_n_auditoria = objetivoControl.mto_n_auditoria;
            this.mto_n_objetivo = objetivoControl.mto_n_objetivo;
            this.mto_n_ciclo = objetivoControl.mto_n_ciclo;
            this.mto_objetivo = objetivoControl.mto_objetivo;
            this.mto_c_categoria = objetivoControl.mto_c_categoria;
            this.mto_usuario = objetivoControl.mto_usuario;
            this.mto_f_graba = objetivoControl.mto_f_graba;
            this.mto_p_maximo = objetivoControl.mto_p_maximo;
            this.mto_califica = objetivoControl.mto_califica;
            this.mto_valoracion = objetivoControl.mto_valoracion;
            this.mto_c_riesgo = objetivoControl.mto_c_riesgo
        } else {
            this.mto_c_empresa = null;
            this.mto_n_auditoria = null;
            this.mto_n_objetivo = null;
            this.mto_n_ciclo = null;
            this.mto_objetivo = null;
            this.mto_c_categoria = null;
            this.mto_usuario = null;
            this.mto_f_graba = null;
            this.mto_p_maximo = null;
            this.mto_califica = null;
            this.mto_valoracion = null;
            this.mto_c_riesgo = null
        }
    }

}