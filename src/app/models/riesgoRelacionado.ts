
export class RiesgoRelacionado {
    mtr_c_empresa: number;
    mtr_n_auditoria: number;
    mtr_n_riesgo: number;
    mtr_n_objetivo: number;
    mtr_riesgo: string;
    mtr_accion: string;
    mtr_usuario: string;
    mtr_f_graba: string

    constructor(riesgoRelacionado?: {
        mtr_c_empresa: number;
        mtr_n_auditoria: number;
        mtr_n_riesgo: number;
        mtr_n_objetivo: number;
        mtr_riesgo: string;
        mtr_accion: string;
        mtr_usuario: string;
        mtr_f_graba: string
    }) {
        if (riesgoRelacionado) {
            this.mtr_c_empresa = riesgoRelacionado.mtr_c_empresa;
            this.mtr_n_auditoria = riesgoRelacionado.mtr_n_auditoria;
            this.mtr_n_riesgo = riesgoRelacionado.mtr_n_riesgo;
            this.mtr_n_objetivo = riesgoRelacionado.mtr_n_objetivo;
            this.mtr_riesgo = riesgoRelacionado.mtr_riesgo;
            this.mtr_accion = riesgoRelacionado.mtr_accion;
            this.mtr_usuario = riesgoRelacionado.mtr_usuario;
            this.mtr_f_graba = riesgoRelacionado.mtr_f_graba
        } else {
            this.mtr_c_empresa = null;
            this.mtr_n_auditoria = null;
            this.mtr_n_riesgo = null;
            this.mtr_n_objetivo = null;
            this.mtr_riesgo = null;
            this.mtr_accion = null;
            this.mtr_usuario = null;
            this.mtr_f_graba = null
        }
    }

}