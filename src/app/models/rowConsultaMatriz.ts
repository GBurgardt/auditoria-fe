
export class RowConsultaMatriz {
    n_registro: number;
    t_registro: string;
    mto_n_ciclo: number;
    mto_n_objetivo: number;
    mto_objetivo: string;
    co_descrip: string;
    mtr_n_riesgo: number;
    mtr_riesgo: string;
    mtr_accion: string;
    mta_n_actividad: number;
    mta_actividad: string;
    mta_existe: string;
    mta_existe_descrip: string;
    mta_norma: string;
    mta_norma_descrip: string;
    mta_ac_estado: string;
    mta_referencia: string;
    columna_c: string;
    mtt_n_tarea: number;
    observac: string;
    children: any;

    constructor(accionControl?: {
        n_registro: number;
        t_registro: string;
        mto_n_ciclo: number;
        mto_n_objetivo: number;
        mto_objetivo: string;
        co_descrip: string;
        mtr_n_riesgo: number;
        mtr_riesgo: string;
        mtr_accion: string;
        mta_n_actividad: number;
        mta_actividad: string;
        mta_existe: string;
        mta_existe_descrip: string;
        mta_norma: string;
        mta_norma_descrip: string;
        mta_ac_estado: string;
        mta_referencia: string;
        columna_c: string;
        mtt_n_tarea: number;
        observac: string;
        children: any
    }) {
        if (accionControl) {
            this.n_registro = accionControl.n_registro;
            this.t_registro = accionControl.t_registro;
            this.mto_n_ciclo = accionControl.mto_n_ciclo;
            this.mto_n_objetivo = accionControl.mto_n_objetivo;
            this.mto_objetivo = accionControl.mto_objetivo;
            this.co_descrip = accionControl.co_descrip;
            this.mtr_n_riesgo = accionControl.mtr_n_riesgo;
            this.mtr_riesgo = accionControl.mtr_riesgo;
            this.mtr_accion = accionControl.mtr_accion;
            this.mta_n_actividad = accionControl.mta_n_actividad;
            this.mta_actividad = accionControl.mta_actividad;
            this.mta_existe = accionControl.mta_existe;
            this.mta_existe_descrip = accionControl.mta_existe_descrip;
            this.mta_norma = accionControl.mta_norma;
            this.mta_norma_descrip = accionControl.mta_norma_descrip;
            this.mta_ac_estado = accionControl.mta_ac_estado;
            this.mta_referencia = accionControl.mta_referencia;
            this.columna_c = accionControl.columna_c;
            this.mtt_n_tarea = accionControl.mtt_n_tarea;
            this.observac = accionControl.observac;
            this.children = accionControl.children
        } else {
            this.n_registro = null;
            this.t_registro = null;
            this.mto_n_ciclo = null;
            this.mto_n_objetivo = null;
            this.mto_objetivo = null;
            this.co_descrip = null;
            this.mtr_n_riesgo = null;
            this.mtr_riesgo = null;
            this.mtr_accion = null;
            this.mta_n_actividad = null;
            this.mta_actividad = null;
            this.mta_existe = null;
            this.mta_existe_descrip = null;
            this.mta_norma = null;
            this.mta_norma_descrip = null;
            this.mta_ac_estado = null;
            this.mta_referencia = null;
            this.columna_c = null;
            this.mtt_n_tarea = null;
            this.observac = null;
            this.children = null
        }
    }

}