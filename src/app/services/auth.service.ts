import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LocalStorageService } from './local-storage.service';
import { getNumberOfCurrencyDigits } from '@angular/common';
import { restResources } from '../constants/restResources';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private http: HttpClient,
        private localStorageService: LocalStorageService
    ) { }

    login = (empresa, nombre, clave) => this.http.post(
        `${environment.WS_URL}/usuarios/login`,
        { empresa, nombre, clave }
    )


    ///////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////// Reusable /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param nombre Nombre del service
     * @param queryParams Parametros query del service
     */
    getResource = (nombre, queryParams) => this.http.get(
        `${environment.WS_URL}/${nombre}`,
        {
            params: new HttpParams({
                fromObject: queryParams
            }),
            headers: new HttpHeaders({
                'x-access-token': this.localStorageService.getToken()
            })
        }
    )

    /**
     * @param nombre Nombre del service
     * @param queryParams Parametros query del service
     */
    postResource = (nombre, recurso) => this.http.post(
        `${environment.WS_URL}/${nombre}`,
        this.getBodyToPostAndPut(nombre, recurso),
        {
            headers: new HttpHeaders({
                'x-access-token': this.localStorageService.getToken()
            })
        }
    )

    /**
     * @param nombre Nombre del service
     * @param queryParams Parametros query del service
     */
    putResource = (nombre, recurso) => this.http.put(
        `${environment.WS_URL}/${nombre}`,
        this.getBodyToPostAndPut(nombre, recurso),
        {
            headers: new HttpHeaders({
                'x-access-token': this.localStorageService.getToken()
            })
        }
    )

    /**
     * @param nombre Nombre del service
     * @param queryParams Parametros query del service
     */
    deleteResource = (nombre, pathParams: string[], queryParams) => this.http.delete(
        `${environment.WS_URL}/${nombre}/${pathParams.join('/')}`,
        {
            params: new HttpParams({
                fromObject: queryParams
            }),
            headers: new HttpHeaders({
                'x-access-token': this.localStorageService.getToken()
            })
        }
    )


    ///////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////// Equivalencias POST -> Body ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    private getBodyToPostAndPut = (endpoint, recurso) => {

        if (endpoint === restResources.OBJETIVOS_CONTROL) {
            return {
                "pmto_c_empresa": this.localStorageService.getEmpresa(),
                "pmto_n_auditoria": this.localStorageService.getNroAuditoria(),
                "pmto_n_objetivo": recurso.mto_n_objetivo,
                "pmto_n_ciclo": recurso.mto_n_ciclo,
                "pmto_objetivo": recurso.mto_objetivo,
                "pmto_c_categoria": recurso.mto_c_categoria,
                "pmto_usuario": this.localStorageService.getUsuario(),
                "pmto_califica": recurso.mto_califica,
                "pmto_p_maximo": recurso.mto_p_maximo,
                "pmto_valoracion": recurso.mto_valoracion,
                "pmto_c_riesgo": recurso.mto_c_riesgo
            }
        }

        if (endpoint === restResources.RIESGOS_RELACIONADOS) {
            return {
                "p_c_empresa": this.localStorageService.getEmpresa(),
                "p_n_auditoria": this.localStorageService.getNroAuditoria(),
                "p_n_riesgo": recurso.mtr_n_riesgo,
                "p_n_objetivo": recurso.mtr_n_objetivo,
                "p_riesgo": recurso.mtr_riesgo,
                "p_accion": recurso.mtr_accion,
                "p_usuario": this.localStorageService.getUsuario()
            }
        }

        /* TODO: En código viejo, para campo p_existe hace esto (AltasAc, Linea 500):
            if (AuditoriaInterna.t_auditoria.equals("O")){
                if (chkExisteControl.getValue()){
                    matriz.setExiste("S");
                }else{
                    matriz.setExiste("N");
                }
            }else{
                if (radioButtonSi.getValue()){
                    existe = "S";
                }
                if (radioButtonNo.getValue()){
                    existe = "N";
                }
                if (radioButtonParcial.getValue()){
                    existe = "P";
                }
                if (radioButtonNoAplica.getValue()){
                    existe = "A";
                }
                matriz.setExiste(existe);
            }
            Cuando la Auditoria es de sistema, al parecer hay radioButtons extras..
        */
        if (endpoint === restResources.ACCIONES_CONTROL) {
            return {
                "p_c_empresa": this.localStorageService.getEmpresa(),
                "p_n_auditoria": this.localStorageService.getNroAuditoria(),
                "p_n_actividad": recurso.mta_n_actividad,
                "p_n_objetivo": recurso.mta_n_objetivo,
                "p_actividad": recurso.mta_actividad,
                "p_existe":
                    this.localStorageService.getTipoAuditoria() === 'O' ?
                        recurso.mta_existe ? 'S' : 'N'
                        :
                        '?',
                "p_existe_descrip": recurso.mta_existe_descrip,
                "p_norma": recurso.mta_norma ? 'S' : 'N',
                "p_norma_descrip": recurso.mta_norma_descrip,
                "p_referencia": recurso.mta_referencia,
                "p_ac_estado": recurso.mta_ac_estado,
                "p_usuario": this.localStorageService.getUsuario(),
                "p_n_riesgo": recurso.mta_n_riesgo
            }
        }

    }

}
