import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Auditoria } from '../models/auditoria';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    auditoriaSubject: Subject<Auditoria> = new Subject();

    constructor() { }

    /**
     * Setear algo en el localStorage, puede ser un json
     */
    setObject = (key, value) => {
        localStorage.setItem(key, JSON.stringify(value));
    }


    /**
     * Obtener algo del localStorage
     */
    getObject = (key) => {
        var value = localStorage.getItem(key);
        return value && JSON.parse(value);
    }

    /**
     * Limpia el local storage
     */
    clearLocalStorage = () => {
        localStorage.clear();
    }


    getEmpresa = () => this.getObject('empresa');
    getUsuario = () => this.getObject('usuario');
    getToken = () => this.getObject('token');
    // getNroAuditoria = () => localStorage.getItem('auditoria');
    getNroAuditoria = () => this.getObject('auditoria') ? this.getObject('auditoria').au_n_auditoria : null;
    getTipoAuditoria = () => this.getObject('auditoria') ? this.getObject('auditoria').au_t_auditoria : null;

    /**
     * Esto permite 'escuchar' los cambios de la auditoria
     * en el onChange del desplegable de las auditorias, nexteo cuando va cambiando
     * y escucho en consulta-matriz para actualizar desplegables
     */
    getAuditoriaSubject = () => this.auditoriaSubject;
}
