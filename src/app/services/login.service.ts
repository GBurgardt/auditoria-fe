import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { LocalStorageService } from './local-storage.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(
        private authService: AuthService,
        private localStorageService: LocalStorageService
    ) { }

    /**
     * Loguea al usuario, persiste el token y retorna data importante
     */
    processLogin = ({ empresa, nombre, clave }) => 
    
        this.authService.login(empresa, nombre, clave)
            .pipe(
                map((resp: any) => {
                    // Si logueó bien guardo el token en el localStorage, y la empresa y el nombre
                    if (resp.datos.auth) {
                        this.localStorageService.setObject('token', resp.datos.token);
                        this.localStorageService.setObject('empresa', empresa);
                        this.localStorageService.setObject('usuario', nombre);
                    }

                    // Retorno data relevante
                    return {
                        isOkey: resp.datos.auth,
                        dataModal: {
                            title: resp.control.descripcion,
                            innerText: resp.control.descripcionLarga
                        }
                    }
                }),
                catchError(val => 
                    of({
                        isOkey: val.error && val.error.datos && val.error.datos.auth,
                        dataModal: {
                            title: val.error.control.descripcion,
                            innerText: val.error.control.descripcionLarga
                        }
                    })
                )
            )
}
