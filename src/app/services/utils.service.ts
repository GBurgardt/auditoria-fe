import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    constructor() { }

    /**
     * Compara dos recursos genéricos. Recibe dos ids (en general)
     * Se usa en los selects
     */
    onSimpleCompareRecurso = (r1, r2) => r1 && r2 && String(r1) === String(r2)
}
